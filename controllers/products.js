const Product=require("../models/product");

exports.getAddProduct = (req, res, next) => {
    res.render("add-product", {
        pageTitle: "Add Product",
        activeAddProduct: true,
        path: "admin/add-product"
    });
    // res.sendFile(path.join(rootpath, "views", "add-product.html"));
};
exports.getPostProduct = (req, res, next) => {
    const product = new Product(req.body.title);
    product.save();
    res.redirect("/");
};
exports.getProducts = (req, res, next) => {
    Product.fetchAll((products) => {
        res.render("shop", {
            prods: products,
            pageTitle: "My Shop",
            activeShop: true,
            path: "/",
        });
    });
};