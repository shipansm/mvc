const express = require("express");
const route = express.Router();
const bodyParser = require("body-parser");
const productsController=require("../controllers/products");
let urlencodeParser = bodyParser.urlencoded({
    extended: true
});
route.get("/add-product",productsController.getAddProduct);
route.post("/add-product", urlencodeParser, productsController.getPostProduct);
module.exports=route;